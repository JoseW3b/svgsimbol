* Browser support for SVG symbol sprites
https://fvsch.com/code/svg-icons/symbol-sprite/

* xlink:href deprecated
http://www.w3.org/TR/SVG2/linking.html

* Pros and con
https://una.im/svg-icons/

* gulp (generador de sprites)
https://github.com/w0rm/gulp-svgstore
https://github.com/jkphl/gulp-svg-sprite

* Error xlink:href angular2
http://stackoverflow.com/questions/35082657/angular2-svg-xlinkhref

* Shadow DOM
https://tympanus.net/codrops/2015/07/16/styling-svg-use-content-css/


xlink:href -> deprecated cambiará a href solo. En safari ios10 solo funciona si lleva xlink


* Links icon set
http://www.flaticon.com
http://www.flaticon.com/packs
https://icomoon.io/#icons
http://glyph.smarticons.co/
http://svgicons.sparkk.fr/
https://useiconic.com/open/

