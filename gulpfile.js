var gulp = require("gulp");
var svgstore = require('gulp-svgstore');
var svgSprite = require('gulp-svg-sprite');
var svgmin = require('gulp-svgmin');
var path = require('path');

gulp.task("svgsprite", function () {
    return gulp.src("**/*.svg", { cwd: "./icons" })
        .pipe(svgSprite({
            mode: {
                symbol: { // symbol mode to build the SVG
                    render: {
                        css: false, // CSS output option for icon sizing
                        scss: false // SCSS output option for icon sizing
                    },
                    dest: 'lib', // destination folder
                    prefix: '.svg--%s', // BEM-style prefix if styles rendered
                    sprite: 'sprite.svg', //generated sprite name
                    example: true // Build a sample page, please!
                }
            }
        }))
        .pipe(gulp.dest("./"))
});

gulp.task("svgstore", function () {
    return gulp.src("./icons/**/*.svg", { base: "./icons"})
        .pipe(svgmin(function (file) {
            var prefix = path.basename(file.relative, path.extname(file.relative));
            return {
                plugins: [{
                    cleanupIDs: {
                        prefix: prefix + "-",
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(gulp.dest("./lib"))
});

// gulp.task('default', ["svgstore"]);